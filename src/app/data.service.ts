import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DataService {

  private goals = new BehaviorSubject<any>(['lifegoal 1', 'lifegoal 2']);
  goal = this.goals.asObservable();

  constructor() { }

  // custom method acessable from other components
  changeGoal(goal) {
    this.goals.next(goal);
  }

}
