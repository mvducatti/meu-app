import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { InicialComponent } from './inicial/inicial.component';
import { DataService } from './data.service';
import { NavbarComponent } from './navbar/navbar.component';
import { PrimeiraLayerComponent } from './primeira-layer/primeira-layer.component';
import { DescricaoinicialComponent } from './descricaoinicial/descricaoinicial.component';
import { ProcessadoraComponent } from './processadora/processadora.component';
import { FacildepagarComponent } from './facildepagar/facildepagar.component';
import { SegundaLayerComponent } from './segunda-layer/segunda-layer.component';
import { TerceiraLayerComponent } from './terceira-layer/terceira-layer.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    InicialComponent,
    NavbarComponent,
    PrimeiraLayerComponent,
    DescricaoinicialComponent,
    ProcessadoraComponent,
    FacildepagarComponent,
    SegundaLayerComponent,
    TerceiraLayerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
