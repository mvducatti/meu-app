import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescricaoinicialComponent } from './descricaoinicial.component';

describe('DescricaoinicialComponent', () => {
  let component: DescricaoinicialComponent;
  let fixture: ComponentFixture<DescricaoinicialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescricaoinicialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescricaoinicialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
