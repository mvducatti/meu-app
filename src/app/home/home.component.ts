import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

// use interpolation to display the values below

export class HomeComponent implements OnInit {

  itemCount: number;
  btnText = 'Add this life goal';
  goalText = '';
  goals = [];

  constructor(private _data: DataService) { }

 // lifecicle hook, whichs is initiated when the app or the component itself loads
  ngOnInit() {
    this._data.goal.subscribe(res => this.goals = res);
    this.itemCount = this.goals.length;
    this._data.changeGoal(this.goals);
  }

  addItem() {
    // push adds item into array
    if (this.goalText.length > 0 ) {
    this.goals.push(this.goalText);
    // resets the word after adding item into array
    this.goalText = '';
    // adds to the counter after item is pushed
    this.itemCount = this.goals.length;
    // everytime a goal in added it will update the changeGoal
    this._data.changeGoal(this.goals);
  } else {
    console.log('Campo em branco!');
  }
  }

}
