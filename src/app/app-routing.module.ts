import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components that I want routing for
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { InicialComponent } from './inicial/inicial.component';

// setup paths

const routes: Routes = [
    { path: '', redirectTo: '/inicial', pathMatch: 'full' },
    { path: 'inicial', component: InicialComponent },
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
