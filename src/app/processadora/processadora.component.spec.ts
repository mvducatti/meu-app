import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessadoraComponent } from './processadora.component';

describe('ProcessadoraComponent', () => {
  let component: ProcessadoraComponent;
  let fixture: ComponentFixture<ProcessadoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessadoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessadoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
