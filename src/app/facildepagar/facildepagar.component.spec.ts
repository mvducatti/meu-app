import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacildepagarComponent } from './facildepagar.component';

describe('FacildepagarComponent', () => {
  let component: FacildepagarComponent;
  let fixture: ComponentFixture<FacildepagarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacildepagarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacildepagarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
